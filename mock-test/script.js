const name = document.getElementsByClassName('name')
const email = document.getElementsByClassName('email')
const number = document.getElementsByClassName('number')
const form= document.getElementById('Form')
const errorElement = document.getElementById('error')

form.addEventListener('submit' (e) => {
    let messages=[]
    if(name.value === '' ||  name.value === null){
        messages.push("You need to enter first and last name")
    }
    if(number.value.length > 10 || number.value.length < 10){
        messages.push("You need to enter valid number")
    }

    if(messages.length > 0){
        e.preventDefault()
        errorElement.innerText=messages.join(', ')
    }

})
function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  
  function validate() {
    const $error = $("#error");
    const email = $("#email").val();
    $error.text("");
  
    if (validateEmail(email)) {
      $error.text(email + " is valid :)");
      $error.css("color", "green");
    } else {
      $error.text(email + " is not valid :(");
      $error.css("color", "red");
    }
    return false;
}
  
$("#submit").on("click", validate);
var series = [
    {make: 'make-1', model: 'Beat'},
    {make: 'make-1', model: 'Travera'},
    {make: 'make-1', model: 'Spark'},
    {make: 'make-2', model: '800'},
    {make: 'make-2', model: 'Alto'},
    {make: 'make-2', model: 'WagonR'},
    {make: 'make-2', model: 'Esteem'},
    {make: 'make-2', model: 'SX4'},
    {make: 'make-3', model: 'Indica'},
    {make: 'make-3', model: 'Indigo'},
    {make: 'make-3', model: 'Safari'},
    {make: 'make-3', model: 'Sumo'},
    {make: 'make-4', model: 'Camry'},
    {make: 'make-4', model: 'Etios'},
    {make: 'make-4', model: 'Corolla'},
    {make: 'make-4', model: 'Endeavour'}
]
$(".company").change(function(){
    var company = $(this).val();
    var options =  '<option value=""><strong>Model</strong></option>';
    $(series).each(function(index, value){
        if(value.make == company){
            options += '<option value="'+value.model+'">'+value.model+'</option>';
        }
    });
        
    $('.model').html(options);
});
